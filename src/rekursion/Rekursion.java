package rekursion;

import java.util.*;

public class Rekursion {

    public static void main(String[] args) 
    {
        int i = 0;
        int tal;
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Välj ett tal mellan 5-9.");
        tal = scan.nextInt();
        if (tal > 4 && tal < 10)
        {
            rakna(i,tal);
        }
        else 
        {
            System.out.println("Du måste välja ett tal mellan 5-9.");
        }
    }
    
    public static void rakna(int i, int tal)
    {
        
        
        
        if (tal-i == 1)
        {
            i++;
            System.out.println("När man tagit bort 1, " + i + "gånger. Får man " + (tal-i));     
        }

        else
        {
            i++;
            System.out.println("Tar man bort 1, " + i + "gånger. Får man " + (tal-i));
             
            rakna(i, tal); 
        }
        return;
    }
    
}
